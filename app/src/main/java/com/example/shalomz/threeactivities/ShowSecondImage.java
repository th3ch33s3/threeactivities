package com.example.shalomz.threeactivities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class ShowSecondImage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_second_image);

        ImageView imageView = (ImageView) findViewById(R.id.SecondImageView);
        Button closeSecondImageButton = (Button) findViewById(R.id.CloseSecondImage);
        closeSecondImageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                finish();
            }
        });
    }
}
