package com.example.shalomz.threeactivities;

import android.content.Intent;
import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;

public class MainActivity extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_main);

        Button firstImageButton = (Button) findViewById(R.id.button);
        firstImageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                firstImage();
            }
        });

        Button secondImageButton = (Button)findViewById(R.id.button2);
        secondImageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                secondImage();
            }
        });

        Button thirdImageButton = (Button)findViewById(R.id.button3);
        thirdImageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                thirdImage();
            }
        });
    }
    private void firstImage() {
        Intent launcFirstImage = new Intent(this, ShowFirstImage.class);
        startActivity(launcFirstImage);
    }
    private void secondImage(){
        Intent launchSecondImage = new Intent(this, ShowSecondImage.class);
        startActivity(launchSecondImage);
    }
    private void thirdImage(){
        Intent launchThirdImage = new Intent(this, ShowThirdImage.class);
        startActivity(launchThirdImage);
    }
}
