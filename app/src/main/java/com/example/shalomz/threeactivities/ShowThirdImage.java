package com.example.shalomz.threeactivities;

import android.support.v7.app.AppCompatActivity;
import android.os.Bundle;
import android.view.View;
import android.widget.Button;
import android.widget.ImageView;

public class ShowThirdImage extends AppCompatActivity {

    @Override
    protected void onCreate(Bundle savedInstanceState) {
        super.onCreate(savedInstanceState);
        setContentView(R.layout.activity_show_third_image);

        ImageView imageView = (ImageView) findViewById(R.id.ThirdImageView);
        Button closeThirdImageButton = (Button) findViewById(R.id.CloseThirdImage);
        closeThirdImageButton.setOnClickListener(new View.OnClickListener() {
            public void onClick(View view) {
                finish();
            }
        });

    }
}
